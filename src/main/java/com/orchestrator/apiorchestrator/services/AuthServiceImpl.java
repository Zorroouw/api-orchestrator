package com.orchestrator.apiorchestrator.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orchestrator.apiorchestrator.dto.LoginDTO;
import com.orchestrator.apiorchestrator.dto.UserDTO;
import com.orchestrator.apiorchestrator.feign.request.UserFeignRequest;
import com.orchestrator.apiorchestrator.hystrix.AuthCommand;
import com.orchestrator.apiorchestrator.interfaces.AuthService;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private AuthCommand authCommand;
	
	@Override
	public Boolean login(LoginDTO login) {
		Boolean response = this.authCommand.login(login);
		return response ;
	}

	@Override
	public UserDTO getUser(Integer id) {
	List<UserFeignRequest> feignRequestList = this.authCommand.getAllUser();
	UserFeignRequest feignRequest = this.authCommand.getUser(id);
	for(UserFeignRequest item : feignRequestList) {
		if(item.getId() == id) {
			UserDTO response = this.getUserDTO(feignRequest);
			return response;
		}
	}
	return null;
	}

	private UserDTO getUserDTO(UserFeignRequest feignRequest) {
		UserDTO response = new UserDTO();
		response.setEmail(feignRequest.getEmail());
		response.setName(feignRequest.getName());
		response.setId(feignRequest.getId());
		response.setLogin(feignRequest.getLogin());

		return response;
	}

	@Override
	public List<UserDTO> getAllUser() {
	List<UserFeignRequest> feignRequestList = this.authCommand.getAllUser();
	List<UserDTO> response = this.getAllUserDTOList(feignRequestList);
		return response;
	}

	private List<UserDTO> getAllUserDTOList(List<UserFeignRequest> feignRequestList) {
		List<UserDTO> response = new ArrayList<UserDTO>();
		for(UserFeignRequest item : feignRequestList) {
			response.add(this.getAllUserDTO(item));
		}
		return response;
	}

	private UserDTO getAllUserDTO(UserFeignRequest item) {
		if(item != null) {
			UserDTO response = new UserDTO();
			response.setEmail(item.getEmail());
			response.setName(item.getName());
			response.setId(item.getId());
			response.setLogin(item.getLogin());
			return response;
		}
		return null;
	}
	@Override
	public Boolean addUser(UserDTO dto) {
		List<UserFeignRequest> feignRequestList = this.authCommand.getAllUser();
		for(UserFeignRequest item : feignRequestList) {
			if(item.getId() == dto.getId()) {
				return false;
			}
		}
		Boolean response = this.authCommand.addUser(dto);
		return response;
	}

	@Override
	public Boolean updateUser(Integer id, UserDTO dto) {
		List<UserFeignRequest> feignRequestList = this.authCommand.getAllUser();
		for(UserFeignRequest item : feignRequestList) {
			if(item.getId() == id) {
				Boolean response = this.authCommand.updateUser(id,dto);
				return response;
			}
		}
		return false;
	}

	@Override
	public Boolean deleteUser(Integer id) {
		List<UserFeignRequest> feignRequestList = this.authCommand.getAllUser();
		for(UserFeignRequest item : feignRequestList) {
			if(item.getId() == id) {
				Boolean response = this.authCommand.deleteUser(id);
				return response;
			}
		}
		return false;
	}
}
