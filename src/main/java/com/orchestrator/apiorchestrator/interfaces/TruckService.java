package com.orchestrator.apiorchestrator.interfaces;

import java.util.List;

import com.orchestrator.apiorchestrator.dto.TruckDTO;

public interface TruckService {

	TruckDTO getTruck(Integer id);

	Boolean addTruck(TruckDTO dto);

	Boolean updateTruck(Integer id, TruckDTO dto);

	Boolean deleteTruck(Integer id);

	List<TruckDTO> getAllTruck();

}
