package com.orchestrator.apiorchestrator.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orchestrator.apiorchestrator.dto.TruckDTO;
import com.orchestrator.apiorchestrator.interfaces.TruckService;

@RestController
@RequestMapping
public class TruckController {

	@Autowired
	private TruckService truckService;
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TruckDTO> getTruck(@PathVariable("id") Integer id) {
		TruckDTO response = this.truckService.getTruck(id);
		return new ResponseEntity<TruckDTO>(response, HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TruckDTO>> getAllDriver(){
		List<TruckDTO> response = this.truckService.getAllTruck();
		return new ResponseEntity<List<TruckDTO>>(response ,HttpStatus.OK);
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> addAgreement(@RequestBody TruckDTO dto)  {
		Boolean result = this.truckService.addTruck(dto);
		return new ResponseEntity<Boolean>(result, HttpStatus.CREATED);
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> updateAgreement(@PathVariable ("id") Integer id,@RequestBody TruckDTO dto)  {
		Boolean result = this.truckService.updateTruck(id, dto);
		return new ResponseEntity<Boolean>(result, HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deleteTruck(@PathVariable("id") Integer id) {
		Boolean response = this.truckService.deleteTruck(id);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}
}
