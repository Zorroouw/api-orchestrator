package com.orchestrator.apiorchestrator.feign.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class TrackingFeingRequest {

	private Integer id;
	private String plate;
	private String altitude;
	private String latitude;

}
