package com.orchestrator.apiorchestrator.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.orchestrator.apiorchestrator.dto.DriverDTO;
import com.orchestrator.apiorchestrator.feign.request.DriverFeignRequest;

@FeignClient("mantenedor-drivers")
public interface DriverFeignClient {

	@GetMapping(value = "/driver/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	DriverFeignRequest getDriver(@PathVariable ("id") Integer id);
	
	@GetMapping(value = "/driver/drivers", produces = MediaType.APPLICATION_JSON_VALUE)
	List<DriverFeignRequest> getAllDriver();

	@PostMapping(value = "/driver", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean addDriver(@RequestBody DriverDTO dto);

	@PutMapping(value = "/driver/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean updateDriver(@PathVariable ("id") Integer id,@RequestBody DriverDTO dto);

	@DeleteMapping(value = "/driver/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean deleteDriver(@PathVariable ("id") Integer id);

	

}
