package com.orchestrator.apiorchestrator.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orchestrator.apiorchestrator.dto.TrackingDTO;
import com.orchestrator.apiorchestrator.interfaces.TrackingService;

@RestController
@RequestMapping("/tracking")
		 
public class TrackingController {

	@Autowired
	private TrackingService trackingService;

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TrackingDTO> getTracking(@PathVariable("id") Integer id) {
		TrackingDTO response = this.trackingService.getTracking(id);
		return new ResponseEntity<TrackingDTO>(response, HttpStatus.OK);
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> addTracking(@RequestBody TrackingDTO dto) {
		Boolean response = this.trackingService.addTracking(dto);
		return new ResponseEntity<Boolean>(response, HttpStatus.CREATED);
	}

	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> updateTracking(@PathVariable("id") Integer id, @RequestBody TrackingDTO dto) {
		Boolean response = this.trackingService.updateTracking(id, dto);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deletePerson(@PathVariable("id") Integer id) {
		Boolean response = this.trackingService.deleteTracking(id);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TrackingDTO>> getAllTracking() {
		List<TrackingDTO> response = this.trackingService.getAllTracking();
		return new ResponseEntity<List<TrackingDTO>>(response, HttpStatus.OK);
	}

}
