package com.orchestrator.apiorchestrator.hystrix;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.orchestrator.apiorchestrator.dto.TruckDTO;
import com.orchestrator.apiorchestrator.feign.client.TruckFeignClient;
import com.orchestrator.apiorchestrator.feign.request.TruckFeignRequest;


@Component
public class TruckCommand {
	
	@Autowired
	private TruckFeignClient truckFeignClient;

	@HystrixCommand
	public TruckFeignRequest getTruck(Integer id) {
		TruckFeignRequest response = this.truckFeignClient.getTruck(id); 
		
		return response;
	}

	@HystrixCommand
	public Boolean addTruck(TruckDTO dto) {
		Boolean response = this.truckFeignClient.addTruck(dto);
		return response;
	}

	@HystrixCommand
	public List<TruckFeignRequest> getAllTruck() {
		List<TruckFeignRequest> response = this.truckFeignClient.getAllTruck();
		return response;
	}

	@HystrixCommand
	public Boolean updateDriver(Integer id, TruckDTO dto) {
		Boolean response = this.truckFeignClient.updateTruck(id, dto);
		return response;
	}

	@HystrixCommand
	public Boolean deleteTruck(Integer id) {
		Boolean response = this.truckFeignClient.deleteTruck(id);
		return response;
	}

	
}
