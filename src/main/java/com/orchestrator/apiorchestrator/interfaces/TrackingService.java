package com.orchestrator.apiorchestrator.interfaces;

import java.util.List;

import com.orchestrator.apiorchestrator.dto.TrackingDTO;

public interface TrackingService {

	TrackingDTO getTracking(Integer id);

	Boolean addTracking(TrackingDTO dto);

	Boolean updateTracking(Integer id, TrackingDTO dto);

	Boolean deleteTracking(Integer id);

	List<TrackingDTO> getAllTracking();

}
