package com.orchestrator.apiorchestrator.hystrix;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.orchestrator.apiorchestrator.feign.client.DashboardFeignClient;
import com.orchestrator.apiorchestrator.feign.request.DriverFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.HistoryFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.TruckFeignRequest;

@Component
public class DashboardCommand {

	@Autowired
	private DashboardFeignClient dashboardFeignClient;

	@HystrixCommand
	public List<DriverFeignRequest> getAllDriver() {
		List<DriverFeignRequest> response = this.dashboardFeignClient.getAllDriver();
		return response;
	}

	@HystrixCommand
	public List<TruckFeignRequest> getAllTruck() {
		
		List<TruckFeignRequest> response = this.dashboardFeignClient.getAllTruck();
		return response ;
	}
	
	@HystrixCommand
	public List<HistoryFeignRequest> getAllHistory() {
		List<HistoryFeignRequest> response = this.dashboardFeignClient.getAllHistory();
		return response;
	}

	@HystrixCommand
	public Integer getQuantityDriver() {
		Integer response = this.dashboardFeignClient.getQuantityDriver();
		return response;
	}

	@HystrixCommand
	public Integer getQuantityTruck() {
		Integer response = this.dashboardFeignClient.getQuantityTruck();
		return response;
	}

	@HystrixCommand
	public Integer getQuantityUser() {
		
		Integer response = this.dashboardFeignClient.getQuantityUser();
		return response ;
	}


}
