package com.orchestrator.apiorchestrator.feign.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DriverFeignRequest {
	private Integer id;
	private String name;

}
