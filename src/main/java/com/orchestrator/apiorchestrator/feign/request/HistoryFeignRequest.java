package com.orchestrator.apiorchestrator.feign.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoryFeignRequest {
	private Integer idSalida;
	private Integer idConductor;
	private Integer idCamion;
	private String fechaSalida;
}
