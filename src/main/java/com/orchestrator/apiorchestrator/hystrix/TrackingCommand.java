package com.orchestrator.apiorchestrator.hystrix;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.orchestrator.apiorchestrator.dto.TrackingDTO;
import com.orchestrator.apiorchestrator.feign.client.TrackingFeingClient;
import com.orchestrator.apiorchestrator.feign.request.TrackingFeingRequest;

@Component
public class TrackingCommand {

	@Autowired
	private TrackingFeingClient trackingFeignClient;

	@HystrixCommand
	public TrackingFeingRequest getTracking(Integer id) {
		TrackingFeingRequest response = this.trackingFeignClient.getTracking(id);
		return response;
	}

	@HystrixCommand
	public List<TrackingFeingRequest> getAllTracking() {
		List<TrackingFeingRequest> response = this.trackingFeignClient.getAllTracking();
		return response;
	}

	@HystrixCommand
	public Boolean addTracking(TrackingDTO dto) {
		Boolean response = this.trackingFeignClient.addTracking(dto);
		return response;
	}

	@HystrixCommand
	public Boolean updateTracking(Integer id, TrackingDTO dto) {
		Boolean response = this.trackingFeignClient.updateTracking(id, dto);
		return response;
	}

	@HystrixCommand
	public Boolean deleteTracking(Integer id) {
		Boolean response = this.trackingFeignClient.deleteTracking(id);
		return response;
	}

}