package com.orchestrator.apiorchestrator.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orchestrator.apiorchestrator.dto.TrackingDTO;
import com.orchestrator.apiorchestrator.feign.request.TrackingFeingRequest;
import com.orchestrator.apiorchestrator.hystrix.TrackingCommand;
import com.orchestrator.apiorchestrator.interfaces.TrackingService;

@Service
public class TrackingServiceImpl implements TrackingService {
	@Autowired
	private TrackingCommand trackingCommand;

	@Override
	public TrackingDTO getTracking(Integer id) {
		List<TrackingFeingRequest> feignRequestList = this.trackingCommand.getAllTracking();
		TrackingFeingRequest feignRequest = this.trackingCommand.getTracking(id);
		for (TrackingFeingRequest item : feignRequestList) {
			if (item.getId() == id) {
				TrackingDTO response = this.getTrackingDTO(feignRequest);
				return response;
			}
		}
		return null;
	}

	private TrackingDTO getTrackingDTO(TrackingFeingRequest feignRequest) {
		TrackingDTO response = new TrackingDTO();
		response.setId(feignRequest.getId());
		response.setPlate(feignRequest.getPlate());
		response.setAltitude(feignRequest.getAltitude());
		response.setLatitude(feignRequest.getLatitude());
		return response;
	}

	@Override
	public List<TrackingDTO> getAllTracking() {
		List<TrackingFeingRequest> feignRequestList = this.trackingCommand.getAllTracking();
		List<TrackingDTO> response = this.getAllTrackingDTOList(feignRequestList);
		return response;
	}

	private List<TrackingDTO> getAllTrackingDTOList(List<TrackingFeingRequest> feignRequestList) {
		List<TrackingDTO> response = new ArrayList<TrackingDTO>();
		for (TrackingFeingRequest item : feignRequestList) {
			response.add(this.getAllTrackingDTO(item));
		}
		return response;
	}

	private TrackingDTO getAllTrackingDTO(TrackingFeingRequest item) {
		if (item != null) {
			TrackingDTO response = new TrackingDTO();
			response.setId(item.getId());
			response.setPlate(item.getPlate());
			response.setAltitude(item.getAltitude());
			response.setLatitude(item.getLatitude());
			return response;
		}
		return null;
	}

	@Override
	public Boolean addTracking(TrackingDTO dto) {
		List<TrackingFeingRequest> feignRequestList = this.trackingCommand.getAllTracking();
		for (TrackingFeingRequest item : feignRequestList) {
			if (item.getId() == dto.getId()) {
				return false;
			}
		}
		Boolean response = this.trackingCommand.addTracking(dto);
		return response;
	}

	@Override
	public Boolean updateTracking(Integer id, TrackingDTO dto) {
		List<TrackingFeingRequest> feignRequestList = this.trackingCommand.getAllTracking();
		for (TrackingFeingRequest item : feignRequestList) {
			if (item.getId() == id) {
				Boolean response = this.trackingCommand.updateTracking(id, dto);
				return response;
			}
		}
		return false;

	}

	@Override
	public Boolean deleteTracking(Integer id) {
		List<TrackingFeingRequest> feignRequestList = this.trackingCommand.getAllTracking();
		for (TrackingFeingRequest item : feignRequestList) {
			if (item.getId() == id) {
				Boolean response = this.trackingCommand.deleteTracking(id);
				return response;
			}
		}
		return false;
	}

}
