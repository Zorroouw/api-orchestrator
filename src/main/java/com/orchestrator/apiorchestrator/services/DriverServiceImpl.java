package com.orchestrator.apiorchestrator.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orchestrator.apiorchestrator.dto.DriverDTO;
import com.orchestrator.apiorchestrator.feign.request.DriverFeignRequest;
import com.orchestrator.apiorchestrator.hystrix.DriverCommand;
import com.orchestrator.apiorchestrator.interfaces.DriverService;

@Service
public class DriverServiceImpl implements DriverService {
	@Autowired
	private DriverCommand driverCommand;

	@Override
	public DriverDTO getDriver(Integer id) {
		List<DriverFeignRequest> feignRequestList = this.driverCommand.getAllDriver();
		DriverFeignRequest feignRequest = this.driverCommand.getDriver(id);
		for (DriverFeignRequest item : feignRequestList) {
			if (item.getId() == id) {
				DriverDTO response = this.getDriverDTO(feignRequest);
				return response;
			}
		}
		return null;
	}

	private DriverDTO getDriverDTO(DriverFeignRequest feignRequest) {
		DriverDTO response = new DriverDTO();
		response.setId(feignRequest.getId());
		response.setName(feignRequest.getName());
		return response;
	}

	@Override
	public List<DriverDTO> getAllDriver() {
		List<DriverFeignRequest> feignRequestList = this.driverCommand.getAllDriver();
		List<DriverDTO> response = this.getAllDriverDTOList(feignRequestList);
		return response;
	}

	private List<DriverDTO> getAllDriverDTOList(List<DriverFeignRequest> feignRequestList) {
		List<DriverDTO> response = new ArrayList<DriverDTO>();
		for (DriverFeignRequest item : feignRequestList) {
			response.add(this.getAllDriverDTO(item));
		}
		return response;
	}

	private DriverDTO getAllDriverDTO(DriverFeignRequest item) {
		if (item != null) {
			DriverDTO response = new DriverDTO();
			response.setId(item.getId());
			response.setName(item.getName());
			return response;
		}
		return null;
	}

	@Override
	public Boolean addDriver(DriverDTO dto) {
		List<DriverFeignRequest> feignRequestList = this.driverCommand.getAllDriver();
		for (DriverFeignRequest item : feignRequestList) {
			if (item.getId() == dto.getId()) {
				return false;
			}
		}
		Boolean response = this.driverCommand.addDriver(dto);
		return response;
	}

	@Override
	public Boolean updateDriver(Integer id, DriverDTO dto) {
		List<DriverFeignRequest> feignRequestList = this.driverCommand.getAllDriver();
		for (DriverFeignRequest item : feignRequestList) {
			if (item.getId() == id) {
				Boolean response = this.driverCommand.updateDriver(id, dto);
				return response;
			}
		}
		return false;

	}

	@Override
	public Boolean deleteDriver(Integer id) {
		List<DriverFeignRequest> feignRequestList = this.driverCommand.getAllDriver();
		for (DriverFeignRequest item : feignRequestList) {
			if (item.getId() == id) {
				Boolean response = this.driverCommand.deleteDriver(id);
				return response;
			}
		}
		return false;
	}

}
