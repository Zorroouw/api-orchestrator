package com.orchestrator.apiorchestrator.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import com.orchestrator.apiorchestrator.feign.request.DriverFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.HistoryFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.TruckFeignRequest;

@FeignClient("api-dashboard")
public interface DashboardFeignClient {

	@GetMapping(value = "/dashboard/drivers", produces = MediaType.APPLICATION_JSON_VALUE)
	List<DriverFeignRequest> getAllDriver();

	@GetMapping(value = "/dashboard/trucks", produces = MediaType.APPLICATION_JSON_VALUE)
	List<TruckFeignRequest> getAllTruck();

	@GetMapping(value = "/dashboard/history", produces = MediaType.APPLICATION_JSON_VALUE)
	List<HistoryFeignRequest> getAllHistory();

	@GetMapping(value = "/dashboard/quantity/drivers", produces = MediaType.APPLICATION_JSON_VALUE)
	Integer getQuantityDriver();

	@GetMapping(value = "/dashboard/quantity/trucks", produces = MediaType.APPLICATION_JSON_VALUE)
	Integer getQuantityTruck();

	@GetMapping(value = "/dashboard/quantity/users", produces = MediaType.APPLICATION_JSON_VALUE)
	Integer getQuantityUser();

}
