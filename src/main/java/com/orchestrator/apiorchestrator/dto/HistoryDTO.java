package com.orchestrator.apiorchestrator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoryDTO {
	private Integer idSalida;
	private Integer idConductor;
	private Integer idCamion;
	private String fechaSalida;

}
