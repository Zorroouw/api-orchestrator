package com.orchestrator.apiorchestrator.services;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orchestrator.apiorchestrator.dto.TruckDTO;
import com.orchestrator.apiorchestrator.feign.request.TruckFeignRequest;
import com.orchestrator.apiorchestrator.hystrix.TruckCommand;
import com.orchestrator.apiorchestrator.interfaces.TruckService;

@Service
public class TruckServiceImpl implements TruckService{

	@Autowired
	private TruckCommand truckCommand;
	
	@Override
	public TruckDTO getTruck(Integer id) {

		List<TruckFeignRequest> feignRequestList = this.truckCommand.getAllTruck();
		TruckFeignRequest feignRequest = this.truckCommand.getTruck(id);
		for(TruckFeignRequest item : feignRequestList) {
			if(item.getId() == id) {
				TruckDTO response = this.getTruckDTO(feignRequest);
				return response;
			}
		}
		return null;
	}

	private TruckDTO getTruckDTO(TruckFeignRequest feignRequest) {
		TruckDTO response = new TruckDTO();
		response.setId(feignRequest.getId());
		response.setPlate(feignRequest.getPlate());
		response.setBrand(feignRequest.getBrand());
		response.setModel(feignRequest.getModel());
		
		return response;
	}

	@Override
	public Boolean addTruck(TruckDTO dto) {
		List<TruckFeignRequest> feignRequestList = this.truckCommand.getAllTruck();
		for(TruckFeignRequest item : feignRequestList) {
			if(item.getId() == dto.getId()) {
				return false;
			}
		}
		Boolean response = this.truckCommand.addTruck(dto);
		return response;
	}

	@Override
	public Boolean updateTruck(Integer id, TruckDTO dto) {
		List<TruckFeignRequest> feignRequestList = this.truckCommand.getAllTruck();
		for(TruckFeignRequest item : feignRequestList) {
			if(item.getId() == id) {
				Boolean response = this.truckCommand.updateDriver(id,dto);
				return response;
			}
		}
		return false;
	}

	@Override
	public Boolean deleteTruck(Integer id) {
		List<TruckFeignRequest> feignRequestList = this.truckCommand.getAllTruck();
		for(TruckFeignRequest item : feignRequestList) {
			if(item.getId() == id) {
				Boolean response = this.truckCommand.deleteTruck(id);
				return response;
			}
		}
		return false;
	}

	@Override
	public List<TruckDTO> getAllTruck() {
		List<TruckFeignRequest> feignRequestList = this.truckCommand.getAllTruck();
		List<TruckDTO> response = this.getAllTruckDTOList(feignRequestList);
		return response;
	}

	private List<TruckDTO> getAllTruckDTOList(List<TruckFeignRequest> feignRequestList) {
		List<TruckDTO> response = new ArrayList<TruckDTO>();
		for(TruckFeignRequest item : feignRequestList) {
			response.add(this.getAllTruckDTOList(item));
		}
		return response;
	}

	private TruckDTO getAllTruckDTOList(TruckFeignRequest item) {
		if(item != null) {
			TruckDTO response = new TruckDTO();

			response.setId(item.getId());
			response.setPlate(item.getPlate());
			response.setBrand(item.getBrand());
			response.setModel(item.getModel());
			return response;
		}
		return null;
	}

}
