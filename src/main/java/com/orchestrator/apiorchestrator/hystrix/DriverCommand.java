package com.orchestrator.apiorchestrator.hystrix;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.orchestrator.apiorchestrator.dto.DriverDTO;
import com.orchestrator.apiorchestrator.feign.client.DriverFeignClient;
import com.orchestrator.apiorchestrator.feign.request.DriverFeignRequest;

@Component
public class DriverCommand {

	@Autowired
	private DriverFeignClient driverFeignClient;

	@HystrixCommand
	public DriverFeignRequest getDriver(Integer id) {
		
		DriverFeignRequest response = this.driverFeignClient.getDriver(id);
		return response;
	}
	
	@HystrixCommand
	public List<DriverFeignRequest> getAllDriver() {
		List<DriverFeignRequest> response = this.driverFeignClient.getAllDriver();
		return response;
	}

	@HystrixCommand
	public Boolean addDriver(DriverDTO dto) {
		Boolean response = this.driverFeignClient.addDriver(dto);
		return response;
	}

	@HystrixCommand
	public Boolean updateDriver(Integer id, DriverDTO dto) {
		Boolean response = this.driverFeignClient.updateDriver(id,dto);
		return response;
	}

	@HystrixCommand
	public Boolean deleteDriver(Integer id) {
		Boolean response = this.driverFeignClient.deleteDriver(id);
		return response;
	}

	

}
