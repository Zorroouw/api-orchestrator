package com.orchestrator.apiorchestrator.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orchestrator.apiorchestrator.dto.DriverDTO;
import com.orchestrator.apiorchestrator.dto.HistoryDTO;
import com.orchestrator.apiorchestrator.dto.TruckDTO;
import com.orchestrator.apiorchestrator.feign.request.DriverFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.HistoryFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.TruckFeignRequest;
import com.orchestrator.apiorchestrator.hystrix.DashboardCommand;
import com.orchestrator.apiorchestrator.interfaces.DashboardService;

@Service
public class DashboardServiceImpl implements DashboardService{

	@Autowired
	private DashboardCommand dashboardCommand;

	@Override
	public List<DriverDTO> getAllDriver() {
		List<DriverFeignRequest> feignRequestList = this.dashboardCommand.getAllDriver();
		List<DriverDTO> response = this.getAllDriverDTOList(feignRequestList);
		return response;
	}

	private List<DriverDTO> getAllDriverDTOList(List<DriverFeignRequest> feignRequestList) {
		List<DriverDTO> response = new ArrayList<DriverDTO>();
		for(DriverFeignRequest item : feignRequestList) {
			response.add(this.getAllDriverDTO(item));
		}
		return response;
	}

	private DriverDTO getAllDriverDTO(DriverFeignRequest item) {
		if(item != null) {
			DriverDTO response = new DriverDTO();
			response.setId(item.getId());
			response.setName(item.getName());
			return response;
		}
		return null;	
	}

	@Override
	public List<TruckDTO> getAllTruck() {
		List<TruckFeignRequest> feignRequestList = this.dashboardCommand.getAllTruck();
		List<TruckDTO> response = this.getAllTruckDTOList(feignRequestList);
		return response;
	}

	private List<TruckDTO> getAllTruckDTOList(List<TruckFeignRequest> feignRequestList) {
		List<TruckDTO> response = new ArrayList<TruckDTO>();
		for(TruckFeignRequest item : feignRequestList) {
			response.add(this.getAllTruckDTO(item));
		}
		return response;
	}

	private TruckDTO getAllTruckDTO(TruckFeignRequest item) {
		if(item != null) {
			TruckDTO response = new TruckDTO();
			response.setId(item.getId());
			response.setPlate(item.getPlate());
			response.setModel(item.getModel());
			response.setBrand(item.getBrand());
			return response;
		}
		return null;
	}

	@Override
	public List<HistoryDTO> getAllHistory() {
		List<HistoryFeignRequest> feignRequestList = this.dashboardCommand.getAllHistory();
		List<HistoryDTO> response = this.getAllHistoryDTOList(feignRequestList);
		return response;
	}

	private List<HistoryDTO> getAllHistoryDTOList(List<HistoryFeignRequest> feignRequestList) {
		List<HistoryDTO> response = new ArrayList<HistoryDTO>();
		for(HistoryFeignRequest item : feignRequestList) {
			response.add(this.getAllHistoryDTO(item));
		}
		return response;
	}

	private HistoryDTO getAllHistoryDTO(HistoryFeignRequest item) {
		if(item != null) {
			HistoryDTO response = new HistoryDTO();
			response.setIdSalida(item.getIdSalida());
			response.setIdConductor(item.getIdConductor());
			response.setIdCamion(item.getIdCamion());
			response.setFechaSalida(item.getFechaSalida());
			return response;
		}
		return null;
	}

	@Override
	public Integer getQuantityDriver() {
		Integer response = this.dashboardCommand.getQuantityDriver();
		return response;
	}

	@Override
	public Integer getQuantityTruck() {
		Integer response = this.dashboardCommand.getQuantityTruck();
		return response;
	}

	@Override
	public Integer getQuantityUser() {
		
		Integer response = this.dashboardCommand.getQuantityUser();
		return response;
	}


}
