package com.orchestrator.apiorchestrator.interfaces;

import java.util.List;

import com.orchestrator.apiorchestrator.dto.DriverDTO;

public interface DriverService {

	DriverDTO getDriver(Integer id);

	Boolean addDriver(DriverDTO dto);

	Boolean updateDriver(Integer id, DriverDTO dto);

	Boolean deleteDriver(Integer id);

	List<DriverDTO> getAllDriver();

}
