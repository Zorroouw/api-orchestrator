package com.orchestrator.apiorchestrator.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.orchestrator.apiorchestrator.dto.DriverDTO;
import com.orchestrator.apiorchestrator.dto.HistoryDTO;
import com.orchestrator.apiorchestrator.dto.TruckDTO;
import com.orchestrator.apiorchestrator.interfaces.DashboardService;

@RestController
@RequestMapping("/dashboard")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class DashboardController {
	
	@Autowired
	private DashboardService dashboardService;
	
	@GetMapping(value = "/drivers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DriverDTO>> getAllDriver(){
		
		List<DriverDTO> response = this.dashboardService.getAllDriver();
		return new ResponseEntity<List<DriverDTO>>(response , HttpStatus.OK);
	}
	
	@GetMapping(value = "/trucks", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TruckDTO>> getAllTruck(){
		  
		List<TruckDTO> response = this.dashboardService.getAllTruck();
		return new ResponseEntity<List<TruckDTO>>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/history", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HistoryDTO>> getAllHistory(){
		
		List<HistoryDTO> response = this.dashboardService.getAllHistory();
		return new ResponseEntity<List<HistoryDTO>>(response ,HttpStatus.OK);
	}
	
	@GetMapping(value = "/quantity/drivers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> getQuantityDriver(){
		
		Integer response = this.dashboardService.getQuantityDriver();
		return new ResponseEntity<Integer>(response , HttpStatus.OK);
	}
	
	@GetMapping(value = "/quantity/trucks", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> getQuantityTruck(){
		
		Integer response = this.dashboardService.getQuantityTruck();
		return new ResponseEntity<Integer>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/quantity/users", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> getQuantityUser(){
		
		Integer response = this.dashboardService.getQuantityUser();
		return new ResponseEntity<Integer>(response , HttpStatus.OK);
	}

}
