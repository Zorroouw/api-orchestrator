package com.orchestrator.apiorchestrator.hystrix;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.orchestrator.apiorchestrator.dto.LoginDTO;
import com.orchestrator.apiorchestrator.dto.UserDTO;
import com.orchestrator.apiorchestrator.feign.client.AuthFeignClient;
import com.orchestrator.apiorchestrator.feign.request.LoginFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.UserFeignRequest;

@Component
public class AuthCommand {
	
	@Autowired
	private AuthFeignClient authFeignClient;
	
	@HystrixCommand
	public UserFeignRequest getUser(Integer id) {
		UserFeignRequest response = this.authFeignClient.getUser(id);
		return response;
	}

	public List<UserFeignRequest> getAllUser() {
		List<UserFeignRequest> response = this.authFeignClient.getAllUser()
;		return response;
	}

	public Boolean addUser(UserDTO dto) {
	Boolean response = this.authFeignClient.addUser(dto);
		return response;
	}

	public Boolean updateUser(Integer id, UserDTO dto) {
	Boolean response = this.authFeignClient.updateUser(id,dto);
		return response;
	}

	public Boolean deleteUser(Integer id) {
	Boolean response = this.authFeignClient.deleteUser(id);
		return response;
	}

	public Boolean login(LoginDTO login) {

		LoginFeignRequest loginRequest = this.getLoginFeignRequest(login);
		Boolean response = this.authFeignClient.login(loginRequest);
		return response;	
		}

	private LoginFeignRequest getLoginFeignRequest(LoginDTO login) {
		if (login == null) {
			return null;
		}
		LoginFeignRequest response = new LoginFeignRequest();

		response.setPassword(login.getPassword());
		response.setUserName(login.getUserName());

		return response;
	}
	




	

	
}
