package com.orchestrator.apiorchestrator.interfaces;

import java.util.List;

import com.orchestrator.apiorchestrator.dto.LoginDTO;
import com.orchestrator.apiorchestrator.dto.UserDTO;

public interface AuthService {

	Boolean login(LoginDTO login);

	UserDTO getUser(Integer id);

	List<UserDTO> getAllUser();

	Boolean updateUser(Integer id, UserDTO dto);

	Boolean deleteUser(Integer id);

	Boolean addUser(UserDTO dto);
}
