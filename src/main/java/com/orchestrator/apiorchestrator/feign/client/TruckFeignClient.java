package com.orchestrator.apiorchestrator.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.orchestrator.apiorchestrator.dto.TruckDTO;
import com.orchestrator.apiorchestrator.feign.request.TruckFeignRequest;

@FeignClient("truck")
public interface TruckFeignClient {

	@GetMapping(value = "/truck/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	TruckFeignRequest getTruck(@PathVariable("id") Integer id);

	@PostMapping(value = "/truck", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean addTruck( @RequestBody TruckDTO dto);

	@GetMapping(value = "/truck/trucks", produces = MediaType.APPLICATION_JSON_VALUE)
	List<TruckFeignRequest> getAllTruck();

	@PutMapping(value = "/truck/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean updateTruck(@PathVariable ("id") Integer id, @RequestBody TruckDTO dto);

	@DeleteMapping(value = "/truck/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean deleteTruck(@PathVariable ("id")Integer id);
}
