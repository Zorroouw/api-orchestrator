package com.orchestrator.apiorchestrator.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.orchestrator.apiorchestrator.dto.UserDTO;
import com.orchestrator.apiorchestrator.feign.request.LoginFeignRequest;
import com.orchestrator.apiorchestrator.feign.request.UserFeignRequest;

@FeignClient("auth")
public interface AuthFeignClient {

	@PostMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean login(@RequestBody(required = true) LoginFeignRequest loginRequest);
	
	@GetMapping(value = "/auth/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	UserFeignRequest getUser(@PathVariable ("id") Integer id);
	
	@GetMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
	List<UserFeignRequest> getAllUser();
	
	@PostMapping(value = "/auth/add-user", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean addUser(@RequestBody UserDTO dto);
	
	@PutMapping(value = "/auth/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean updateUser(@PathVariable ("id") Integer id, @RequestBody UserDTO dto);
	
	@DeleteMapping(value = "/auth/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean deleteUser(@PathVariable ("id") Integer id);
}
