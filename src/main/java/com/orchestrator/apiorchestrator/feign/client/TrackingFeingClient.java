package com.orchestrator.apiorchestrator.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.orchestrator.apiorchestrator.dto.TrackingDTO;
import com.orchestrator.apiorchestrator.feign.request.TrackingFeingRequest;

@FeignClient("api-tracking")
public interface TrackingFeingClient {

	@GetMapping(value = "/tracking/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	TrackingFeingRequest getTracking(@PathVariable("id") Integer id);

	@GetMapping(value = "/tracking", produces = MediaType.APPLICATION_JSON_VALUE)
	List<TrackingFeingRequest> getAllTracking();

	@PostMapping(value = "/tracking", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean addTracking(@RequestBody TrackingDTO dto);

	@PutMapping(value = "/tracking/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean updateTracking(@PathVariable("id") Integer id, @RequestBody TrackingDTO dto);

	@DeleteMapping(value = "/tracking/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Boolean deleteTracking(@PathVariable("id") Integer id);

}
