package com.orchestrator.apiorchestrator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrackingDTO {

	private Integer id;
	private String plate;
	private String altitude;
	private String latitude;

}
