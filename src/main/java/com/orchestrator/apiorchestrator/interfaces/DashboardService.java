package com.orchestrator.apiorchestrator.interfaces;

import java.util.List;

import com.orchestrator.apiorchestrator.dto.DriverDTO;
import com.orchestrator.apiorchestrator.dto.HistoryDTO;
import com.orchestrator.apiorchestrator.dto.TruckDTO;

public interface DashboardService {

	List<DriverDTO> getAllDriver();

	List<TruckDTO> getAllTruck();

	List<HistoryDTO> getAllHistory();

	Integer getQuantityDriver();

	Integer getQuantityTruck();

	Integer getQuantityUser();

}
