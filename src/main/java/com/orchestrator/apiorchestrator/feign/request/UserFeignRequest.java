package com.orchestrator.apiorchestrator.feign.request;

import com.orchestrator.apiorchestrator.dto.LoginDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFeignRequest {
	private Integer id;
	private String name;
	private String email;
	private LoginDTO login;
}
